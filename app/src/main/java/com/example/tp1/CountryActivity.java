package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CountryActivity extends AppCompatActivity {
    private TextView capitale;
    private TextView langue;
    private TextView monnaie;
    private TextView pop;
    private TextView superficie;
    private EditText editcap;
    private EditText editlang;
    private EditText editpop;
    private EditText editmonnaie;
    private EditText editsuperficie;
    private TextView country;
    private ImageView flag;
    private Button sauvegarder;
    private String item;
    private CountryList countries;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        capitale = (TextView) findViewById(R.id.cap);
        langue = (TextView) findViewById(R.id.lang);
        monnaie = (TextView) findViewById(R.id.Monnaie);
        pop = (TextView) findViewById(R.id.pop);
        superficie = (TextView) findViewById(R.id.Superficie);
        editcap = (EditText) findViewById(R.id.editcap);
        editlang = (EditText) findViewById(R.id.editlang);
        editpop = (EditText) findViewById(R.id.editpop);
        editmonnaie = (EditText) findViewById(R.id.editmonnaie);
        editsuperficie = (EditText) findViewById(R.id.editsuperficie);

        Intent intent = getIntent();
        item = intent.getStringExtra("item");
        country = (TextView) findViewById(R.id.Country);
        country.setText(item);
        flagchange(item);

        String size=String.valueOf(countries.getCountry(item).getmArea());
        String popu=String.valueOf(countries.getCountry(item).getmPopulation());

        editcap.setText(countries.getCountry(item).getmCapital());
        editlang.setText(countries.getCountry(item).getmLanguage());
        editsuperficie.setText(size);
        editpop.setText(popu);
        editmonnaie.setText(countries.getCountry(item).getmCurrency());


        sauvegarder = (Button) findViewById(R.id.sauvegarde);
        sauvegarder.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Sauvegarde(item,editcap.getText().toString(),editlang.getText().toString(),editmonnaie.getText().toString(),editpop.getText().toString(),editsuperficie.getText().toString());
                finish();
            }
        });

    }

    public void flagchange(String country){
        flag = (ImageView) findViewById(R.id.flag);
        if (country.equals("France"))
            flag.setImageResource(R.drawable.flag_of_france);
        else if (country.equals("Allemagne"))
            flag.setImageResource(R.drawable.flag_of_germany);
        else if (country.equals("Espagne"))
            flag.setImageResource(R.drawable.flag_of_spain);
        else if (country.equals("Afrique du Sud"))
            flag.setImageResource(R.drawable.flag_of_south_africa);
        else if (country.equals("Japon"))
            flag.setImageResource(R.drawable.flag_of_japan);
        else if (country.equals("États-Unis"))
            flag.setImageResource(R.drawable.flag_of_the_united_states);
    }

    public void Sauvegarde(String country,String capitale, String langue, String monnaie, String population, String superficie){
        //int p = Integer.parseInt(population);
        //int s = Integer.parseInt(superficie);
        countries.getCountry(country).setmCapital(capitale);
        countries.getCountry(country).setmLanguage(langue);
        countries.getCountry(country).setmCurrency(monnaie);
        countries.getCountry(country).setmPopulation(2);
        countries.getCountry(country).setmArea(3);
    }

}
